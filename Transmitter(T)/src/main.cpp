#include<Arduino.h>
#include<SPI.h>
#include<nRF24L01.h>
#include<RF24.h>  
#include<Keypad.h>
#include<string.h>

//LiquidCrystal_I2C lcd(0x3f,16,2);

const byte ROWS = 3;
const byte COLS = 4;
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','D','C'},
};
byte rowPins[ROWS] = {6,9,10}; // connect to the row pinouts of the keypad
byte colPins[COLS] = {2,3,4,5};    // connect to the column pinouts of the keypad
Keypad kpad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";


int orderno = 1;

void setup() {
    // put your setup code here, to run once:
    //lcd.begin();
    Serial.begin(9600);
    radio.begin();
    radio.openWritingPipe(address);
    radio.setPALevel(RF24_PA_MIN);
    radio.stopListening();
}

void loop() {
    // put your main code here, to run repeatedly:
    String order1="";
    char letter;
    char order[2];
    int i;
    
    for(i=0;i<2;i++){
        letter = kpad.waitForKey();
        order1 += letter;
    }
    order[0] = order1[0];
    order[1] = order1[1];
    radio.write(&order, sizeof(order));
}