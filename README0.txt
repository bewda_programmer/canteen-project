EPICS PROJECT


*T---->*J----->*D


*
T = Transmitter at the counter where cashier will dial in food codes to be sent over to *J.

J = This will be located inside the kitchen. This arduino will be commmunicating with two arduinos one with *T and with *D. 
    It will be reciving the code and translating it to food item name which will be displayed and at the same time sending the order no. to *D when order is done.

D = This will be the third arduino whose primary task in to Display order no.s that are ready. This 3rd arduino is introduced since the kitchen and the dispatch stations are way to far.

